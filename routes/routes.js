module.exports = (router) => {
  router.get("/", (req, res) => {
    res.json({ works: true });
  });
  require("./metaDataextractor/postRdf")(router);
};