const file = require("file");
const async = require("async");
// config
const winston = require("../../config/winston")(module);

const parseRdf = require("./parseRdf");
const postRdf = async (req, res) => {
  const { db } = req;
  const { folderpath } = req.body;
  console.log("folderpath", req.body);
  const worker = async.queue((path, next) => {
  // parse rdf
    console.log("pathhhh", path);
    parseRdf(path, async (parseError, obj) => {
      if (parseError) {
        winston.error(parseError);
      }
      winston.debug(JSON.stringify(obj));
      try {
        const insertResponse = await db.collection("testBooks").insertOne(obj);
        winston.info(JSON.stringify(insertResponse));
      } catch (insertError) {
        if(insertError.code !==11000) {
          winston.error(JSON.stringify(insertError));
        }
      }
      winston.debug(JSON.stringify(obj));
      next(res.send({response: "process completed"}));
    });
  }, 10);

  file.walk(folderpath, (fileWalkError, dirPath, dirs, files) => {
    if (fileWalkError) {
      winston.error(fileWalkError);
    }
    files.forEach((path) => {
      worker.push(path);
    });
  });

}

module.exports = postRdf;