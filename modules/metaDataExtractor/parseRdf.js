const fs = require("fs");
const cheerio = require("cheerio");
// config
const winston = require("../../config/winston")(module);

const parseRdf = (filepath, cb) => {
  fs.readFile(filepath, (readFileErr, data) => {
    if (readFileErr) {
      winston.error(readFileErr);
      return cb(readFileErr);
    }

    let q = cheerio.load(data.toString());
    let text = (index, elem) => {
      return q(elem).text();
    }
    const bookMetadata = {
      _id: q("pgterms\\:ebook").attr("rdf:about").replace("ebooks/", ""),
      title: q("dcterms\\:title").text(),
      authors: q("pgterms\\:agent pgterms\\:name").map(text).get(),
      publisher: q("dcterms\\:publisher").text(),
      publication_date: q("dcterms\\:issued").text(),
      language: q("dcterms\\:language rdf\\:Description rdf\\:value").text(),
      subjects: q("dcterms\\:subject rdf\\:Description rdf\\:value").map(text).get()
    };
    // winston.debug(JSON.stringify(bookMetadata));
    cb(null, bookMetadata);
  });
}

module.exports = parseRdf;