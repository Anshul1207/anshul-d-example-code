/* eslint-disable no-unused-vars */
/* eslint-disable no-undef */
process.env.NODE_ENV = "testing";
process.env.DBNAME = "testing";
let chai = require("chai");
let chaiHttp = require("chai-http");
let index = require("../index");
let should = chai.should();
chai.use(chaiHttp);
// module
const parseRdf = require("../modules/metaDataExtractor/parseRdf");

describe("metadata extractor", () => {
  describe("/POST folderpath", () => {
    it("should post folderpath and store all the data in database", (done) => {
      chai.request(index)
        .post("/postRdf")
        .send({
          folderpath: "./test/cache"
        })
        .end((err, res) => {
          res.should.have.status(200);
          res.body.should.be.a("object");
          res.body.should.have.property("response").eql("process completed");
          done();
        })
    })
  })

  describe("parse rdf", () => {
    it("parse rdf", async () => {
      const expectedValue = {
        _id:"1",
        title:"The Declaration of Independence of the United States of America",
        authors:["Jefferson, Thomas"],
        publisher:"Project Gutenberg",
        publication_date:"1971-12-01",
        language:"en",
        subjects:[
          "United States. Declaration of Independence",
          "JK",
          "United States -- History -- Revolution, 1775-1783 -- Sources",
          "E201"
        ]
      }
      parseRdf("./test/cache/epub/1/pg1.rdf",(err, obj) => {
        obj.should.have.property("_id").with.eql(expectedValue._id);
        obj.should.have.property("title").with.eql(expectedValue.title);
        obj.should.have.property("authors").with.eql(expectedValue.authors);
        obj.should.have.property("publisher").with.eql(expectedValue.publisher);
        obj.should.have.property("publication_date").with.eql(expectedValue.publication_date);
        obj.should.have.property("language").with.eql(expectedValue.language);
        obj.should.have.property("subjects").with.eql(expectedValue.subjects);

      });
    });
  })
})