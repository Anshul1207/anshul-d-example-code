## Installation Steps

I have attached .env-example to define environment variables
I have sent required env variables in the mail as well.

### Using node version 10

1. npm i
2. update .env file
3. npm start
4. for test - npm test

### postman API documentation
https://documenter.getpostman.com/view/10166078/T17J7m2G

### curl request
curl --location --request POST 'http://localhost:3000/postRdf' \
--header 'Content-Type: application/json' \
--data-raw '{
	"folderpath": "/home/local/INTERNAL/anshul.d/Downloads/test"
}'

Please let me know if you are facing any issue while running the application
1. email - anshul.dharmadhikari@live.com
2. Phone No - +91-8884567675

### Q/A
1. Scalability, how long does it take to index all the content, what sort of memory consumption/network optimisations have you done?
 - In this example I have used async worker to process 10 file paths at a given time.The application can be scaled by implementing messaging queue like rabbitmq we can create the project as a dockerized service and run it on n number of pods where our worker can distribute the tasks for example - 40 containers with 40 filepaths of rdf no of containers can be increased based on CPU and memory usage


2. Querying the dataset, how should the database be configured to search for specific fields quickly?
- Indexing can be done on fields which will increase the searching time but we need to make sure that particular key is not exceeding the index key limit.
