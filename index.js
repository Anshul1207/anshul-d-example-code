const express = require("express");
const bodyParser= require("body-parser");
const logger = require("morgan");
const { MongoClient } = require("mongodb");
// config
const winston = require("./config/winston")(module);


require("dotenv").config();
const app = express();
let db;
app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json({limit: "50mb"}));
app.use(
  function crossOrigin(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Content-Type, Authorization, Content-Length, X-Requested-With");
    return next();
  }
);
app.use(logger("dev"));
app.use(logger("combined", { stream: winston.stream.write }));
// mongodb connect
const mongodbUrl = `mongodb+srv://${process.env.USERNAME}:${process.env.PASSWORD}@cluster0-b06dh.mongodb.net/${process.env.DBNAME}?retryWrites=true&w=majority`;
winston.info("mongodbURL", mongodbUrl);

MongoClient.connect(mongodbUrl, { useNewUrlParser: true,useUnifiedTopology: true }, async (err, client) => {
  if(err) {
    console.log("Error occurred while connecting to MongoDB Atlas...\n",err);
  }
  db = await client.db(process.env.DBNAME);
  console.log("Connected to mongodb database");
//   client.close();
});

const router = express.Router();
router.use(function(req,res,next){
  req.db = db;
  next();
});

require("./routes/routes")(router);
app.use(router);
const port = process.env.PORT || 3000;
app.listen(port, () => winston.info(`Server started on port ${port}`));

module.exports = app;