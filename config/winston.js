const {
  createLogger,
  addColors,
  format,
  transports,
} = require("winston");
const path = require("path");

const config = {
  levels: {
    test   : -1,
    error  : 0,
    info   : 1,
    debug  : 2
  },
  colors: {
    error  : "bold red",
    info   : "bold blue",
    debug  : "bold green",
    timestamp : "bold grey",
    label  : "bold white",
  },
};

const env = process.env.NODE_ENV || "development";

addColors(config.colors);

const getLabel = (file) => {
  const parts = file.filename.split(path.sep);
  return path.join(parts[parts.length - 2], parts.pop());
};

const setLevel = (env) => {
  if (env === "production") {
    return "info";
  }
  if (env === "testing") {
    return "test";
  }
  return "debug";
}
const colorizer = format.colorize();
const winston = (file) => {
  return createLogger({
    levels: config.levels,
    format: format.combine(
      format.label({ label: getLabel(file) }),
      format.timestamp({ format: "YYYY-MM-DD HH:mm:ss" }),
      format.json(),
    ),
    transports: [
      new transports.Console({
        format: format.combine(
          format.printf((info) => {
            const infoTimestamp = colorizer.colorize("timestamp", `${info.timestamp}`);
            const infoLabel = colorizer.colorize("label", `[${info.label}]`);
            const infoLevel = colorizer.colorize(info.level, `[${info.level}]:`);
            return `${infoTimestamp} ${infoLabel} ${infoLevel} ${info.message}`;
          }),
        ),
        level: setLevel(env),
      }),
    ],
  });
}

module.exports = winston;