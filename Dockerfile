FROM node:10-alpine

# USER node
RUN apk add git

WORKDIR /app

COPY package*.json ./
RUN npm install
RUN apk add --no-cache bash
RUN apk add busybox-extras
RUN npm ci
COPY . .
EXPOSE 3000

CMD [ "npm", "start" ]